<?php

namespace App\Helpers;

class Tools
{

    public static function loadJquery()
    {
        return '<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>'."\n\t".'<script>window.jQuery || document.write(\'<script src="' . asset("assets/vendor/jquery/dist/jquery.min.js") . '"><\/script>\')</script>';
    }

    public static function loadJs($path)
    {
        return '<script src="' . asset('assets/'.$path) .'"></script>';
    }

    public static function loadCss($path)
    {
        return '<link rel="stylesheet" href="' . asset('assets/'.$path) . '">';
    }

    public static function isActive($routeName)
    {
        return str_is($routeName, \Route::currentRouteName());
    }

    public static function formataData($data)
    {
        $meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        list($dia, $mes, $ano) = explode('/', $data);

        return $dia . ' de ' . $meses[(int) $mes - 1] . ' de ' . $ano;
    }

    public static function listaEstados()
    {
        return [
            'AC' => 'AC - Acre',
            'AL' => 'AL - Alagoas',
            'AP' => 'AP - Amapá',
            'AM' => 'AM - Amazonas',
            'BA' => 'BA - Bahia',
            'CE' => 'CE - Ceará',
            'DF' => 'DF - Distrito Federal',
            'ES' => 'ES - Espírito Santo',
            'GO' => 'GO - Goiás',
            'MA' => 'MA - Maranhão',
            'MT' => 'MT - Mato Grosso',
            'MS' => 'MS - Mato Grosso do Sul',
            'MG' => 'MG - Minas Gerais',
            'PA' => 'PA - Pará',
            'PB' => 'PB - Paraíba',
            'PR' => 'PR - Paraná',
            'PE' => 'PE - Pernambuco',
            'PI' => 'PI - Piauí',
            'RJ' => 'RJ - Rio de Janeiro',
            'RN' => 'RN - Rio Grande do Norte',
            'RS' => 'RS - Rio Grande do Sul',
            'RO' => 'RO - Rondônia',
            'RR' => 'RR - Roraima',
            'SC' => 'SC - Santa Catarina',
            'SP' => 'SP - São Paulo',
            'SE' => 'SE - Sergipe',
            'TO' => 'TO - Tocantins'
        ];
    }

}
