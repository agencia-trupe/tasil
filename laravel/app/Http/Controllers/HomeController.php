<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Chamadas;
use App\Models\Produto;

class HomeController extends Controller
{
    public function index()
    {
        $banners   = Banner::ordenados()->get();
        $chamadas  = Chamadas::first();
        $promocoes = Produto::where('promocao', 1)->ordenados()->get();
        $destaques = Produto::where('destaque', 1)->ordenados()->get();

        return view('frontend.home', compact('banners', 'chamadas', 'promocoes', 'destaques'));
    }
}
