<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CatalogoRequest;
use App\Http\Controllers\Controller;

use App\Models\Catalogo;

class CatalogoController extends Controller
{
    public function index()
    {
        $registro = Catalogo::first();

        return view('painel.catalogo.edit', compact('registro'));
    }

    public function update(CatalogoRequest $request, Catalogo $registro)
    {
        try {
            $input = $request->all();

            if ($request->hasFile('catalogo')) {
                $input['catalogo'] = Catalogo::uploadFile();
            }

            $registro->update($input);

            return redirect()->route('painel.catalogo.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
