<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RepresentantesRequest;
use App\Http\Controllers\Controller;

use App\Models\Representante;

class RepresentantesController extends Controller
{
    public function index()
    {
        $registros = Representante::ordenados()->get();

        return view('painel.representantes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.representantes.create');
    }

    public function store(RepresentantesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Representante::upload_imagem();

            Representante::create($input);

            return redirect()->route('painel.representantes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Representante $registro)
    {
        return view('painel.representantes.edit', compact('registro'));
    }

    public function update(RepresentantesRequest $request, Representante $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Representante::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.representantes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Representante $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.representantes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
