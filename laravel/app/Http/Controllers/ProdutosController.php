<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Produto;
use App\Models\ProdutoCategoria;
use App\Models\Catalogo;

class ProdutosController extends Controller
{
    public function index(ProdutoCategoria $categoria)
    {
        $categorias = ProdutoCategoria::ordenados()->get();

        if ($categoria->exists) {
            $produtos = Produto::ordenados()->categoria($categoria->id)->get();
        } else {
            $produtos  = Produto::orderByRaw('RAND()')->get();
            $categoria = null;
        }

        $catalogo = Catalogo::first();

        return view('frontend.produtos.index', compact('categorias', 'produtos', 'categoria', 'catalogo'));
    }

    public function show(ProdutoCategoria $categoria, Produto $produto)
    {
        $categorias = ProdutoCategoria::ordenados()->get();

        return view('frontend.produtos.show', compact('categorias', 'categoria', 'produto'));
    }

    public function busca()
    {
        $termo = request('termo');

        if ($termo) {
            $produtos = Produto::ordenados()->busca($termo)->get();

            return view('frontend.produtos.busca', compact('termo', 'produtos'));
        }

        return redirect()->route('produtos');
    }
}
