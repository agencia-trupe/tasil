<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Representante;

class RepresentantesController extends Controller
{
    public function index($estado = null)
    {
        if (!$estado) {
            return view('frontend.representantes', compact('estado'));
        }

        $representantes = Representante::where('estado', $estado)->ordenados()->get();

        return view('frontend.representantes', compact('representantes', 'estado'));
    }
}
