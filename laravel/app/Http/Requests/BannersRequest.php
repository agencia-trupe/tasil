<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'fundo' => 'required|image',
            'imagem' => 'image',
            'texto' => '',
            'link' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['fundo'] = 'image';
        }

        return $rules;
    }
}
