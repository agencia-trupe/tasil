<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChamadasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem_1' => 'image',
            'titulo_1' => 'required',
            'subtitulo_1' => 'required',
            'link_1' => 'required',
            'imagem_2' => 'image',
            'titulo_2' => 'required',
            'subtitulo_2' => 'required',
            'link_2' => 'required',
            'imagem_3' => 'image',
            'titulo_3' => 'required',
            'subtitulo_3' => 'required',
            'link_3' => 'required',
        ];
    }
}
