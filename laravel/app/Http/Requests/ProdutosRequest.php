<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produtos_categoria_id' => 'required',
            'titulo' => 'required',
            'capa' => 'required|image',
            'sugestao_de_uso' => 'required',
            'descricao' => '',
            'dimensoes' => 'required',
            'cor' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
