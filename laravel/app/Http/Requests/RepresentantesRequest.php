<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RepresentantesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'estado' => 'required',
            'imagem' => 'image',
            'titulo' => 'required',
            'informacoes' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
