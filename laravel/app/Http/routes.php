<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('produtos/{categoria_slug?}', 'ProdutosController@index')->name('produtos');
    Route::get('produtos/{categoria_slug}/{produto_slug}', 'ProdutosController@show')->name('produtos.show');
    Route::get('quem-somos', 'QuemSomosController@index')->name('quem-somos');
    Route::get('ferramentaria', 'FerramentariaController@index')->name('ferramentaria');
    Route::get('representantes/{estado?}', 'RepresentantesController@index')->name('representantes');
    Route::get('fale-conosco', 'FaleConoscoController@index')->name('fale-conosco');
    Route::post('fale-conosco', 'FaleConoscoController@post')->name('fale-conosco.post');
    Route::get('busca', 'ProdutosController@busca')->name('busca');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('catalogo', 'CatalogoController', ['only' => ['index', 'update']]);
		Route::resource('produtos/categorias', 'ProdutosCategoriasController', ['parameters' => ['categorias' => 'categorias_produtos']]);
		Route::resource('produtos', 'ProdutosController');
		Route::get('produtos/{produtos}/imagens/clear', [
			'as'   => 'painel.produtos.imagens.clear',
			'uses' => 'ProdutosImagensController@clear'
		]);
		Route::resource('produtos.imagens', 'ProdutosImagensController', ['parameters' => ['imagens' => 'imagens_produtos']]);
		Route::resource('representantes', 'RepresentantesController');
		Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
		Route::resource('chamadas', 'ChamadasController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
