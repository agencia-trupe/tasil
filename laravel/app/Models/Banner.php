<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Banner extends Model
{
    protected $table = 'banners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_fundo()
    {
        return CropImage::make('fundo', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 600,
            'height' => 565,
            'path'   => 'assets/img/banners/'
        ]);
    }
}
