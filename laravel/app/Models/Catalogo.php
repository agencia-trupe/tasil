<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Catalogo extends Model
{
    protected $table = 'catalogo';

    protected $guarded = ['id'];

    public static function uploadFile() {
        $file = request()->file('catalogo');

        $path = 'assets/catalogo/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }
}
