<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoImagem extends Model
{
    protected $table = 'produtos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'bgcolor' => '#fff',
                'path'    => 'assets/img/produtos/imagens/thumbs/'
            ],
            [
                'width'   => 500,
                'height'  => 500,
                'bgcolor' => '#fff',
                'path'    => 'assets/img/produtos/imagens/'
            ]
        ]);
    }
}
