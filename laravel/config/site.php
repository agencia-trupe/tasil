<?php

return [

    'name'        => 'Tasil',
    'title'       => 'Tasil',
    'description' => 'A Tasil tem uma gama de produtos plásticos que você procura. Gande variedade de produtos plásticos para festas e eventos.',
    'keywords'    => 'produtos plásticos, plásticos para festas e eventos, tubos, caixinhas, boleiras, canecas, mini lembrancinhas, baleiro, base, espiral, tela mágica, push cake pop, confete, balde plástico',
    'shareImg'    => '',
    'analytics'   => null

];
