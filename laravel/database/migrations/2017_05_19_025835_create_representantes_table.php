<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentantesTable extends Migration
{
    public function up()
    {
        Schema::create('representantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('estado');
            $table->string('imagem');
            $table->string('titulo');
            $table->text('informacoes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('representantes');
    }
}
