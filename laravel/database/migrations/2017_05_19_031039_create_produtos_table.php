<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produtos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('capa');
            $table->text('sugestao_de_uso');
            $table->text('descricao');
            $table->string('dimensoes');
            $table->string('cor');
            $table->boolean('promocao');
            $table->boolean('destaque');
            $table->foreign('produtos_categoria_id')->references('id')->on('produtos_categorias')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('produtos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('produtos_imagens');
        Schema::drop('produtos');
        Schema::drop('produtos_categorias');
    }
}
