<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogoTable extends Migration
{
    public function up()
    {
        Schema::create('catalogo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('catalogo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('catalogo');
    }
}
