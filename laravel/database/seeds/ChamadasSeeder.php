<?php

use Illuminate\Database\Seeder;

class ChamadasSeeder extends Seeder
{
    public function run()
    {
        DB::table('chamadas')->insert([
            'imagem_1' => '',
            'titulo_1' => '',
            'subtitulo_1' => '',
            'link_1' => '',
            'imagem_2' => '',
            'titulo_2' => '',
            'subtitulo_2' => '',
            'link_2' => '',
            'imagem_3' => '',
            'titulo_3' => '',
            'subtitulo_3' => '',
            'link_3' => '',
        ]);
    }
}
