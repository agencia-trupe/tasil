<?php

use Illuminate\Database\Seeder;

class QuemSomosSeeder extends Seeder
{
    public function run()
    {
        DB::table('quem_somos')->insert([
            'texto' => '',
            'missao' => '',
            'visao' => '',
            'valores' => '',
        ]);
    }
}
