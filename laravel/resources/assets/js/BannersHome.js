export default function BannersHome() {
    $('.banners').cycle({
        slides: '>.slide',
        timeout: 5000,
        pagerTemplate: '<a href=#>{{slideNum}}</a>'
    });

    $('.cycle-pager a').click(function() {
        $('.banners').cycle('pause');
    });
};
