export default function CarouselHome() {
    $('.promocoes-thumbs').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 710,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }]
    });
};
