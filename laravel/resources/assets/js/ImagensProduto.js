export default function ImagensProduto() {
    var $imagem = $('.produto-imagem img'),
        $thumbs = $('.produto-imagens-thumbs a');

    $thumbs.click(function(event) {
        event.preventDefault();

        if ($(this).hasClass('active')) return;

        $thumbs.removeClass('active');
        $(this).addClass('active');

        $imagem.attr('src', $(this).data('imagem'));
    });
};
