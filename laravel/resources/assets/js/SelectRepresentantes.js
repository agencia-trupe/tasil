export default function SelectRepresentantes() {
    $('#representantes-estados').change(function() {
        var uf    = $(this).val(),
            base  = $('base').attr('href');

        if (uf) {
            window.location = base + '/representantes/' + uf;
        } else {
            window.location = base + '/representantes';
        }
    });
};
