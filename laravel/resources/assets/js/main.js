import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import ModalBusca from './ModalBusca';
import FormularioContato from './FormularioContato';
import SelectRepresentantes from './SelectRepresentantes';
import BannersHome from './BannersHome';
import CarouselHome from './CarouselHome';
import ImagensProduto from './ImagensProduto';

AjaxSetup();
MobileToggle();
ModalBusca();
FormularioContato();
SelectRepresentantes();
BannersHome();
CarouselHome();
ImagensProduto();
