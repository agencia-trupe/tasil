    <footer>
        <div class="detalhe-borda">
            <div class="center">
                <div class="cor-1"></div>
                <div class="cor-2"></div>
                <div class="cor-3"></div>
            </div>
        </div>

        <div class="center">
            <div class="wrapper">
                <div class="col">
                    <p class="telefones">
                        @foreach(explode(',', trim($contato->telefones)) as $telefone)
                            <span>
                                @foreach(explode(' ', trim($telefone)) as $part)
                                @if(strlen($part) <= 3)
                                {{ $part }}
                                @else
                                <strong>{{ $part }}</strong>
                                @endif
                                @endforeach
                            </span>
                        @endforeach
                    </p>
                    <p class="whatsapp">
                        @foreach(explode(' ', trim($contato->whatsapp)) as $part)
                        @if(strlen($part) <= 3)
                        {{ $part }}
                        @else
                        <strong>{{ $part }}</strong>
                        @endif
                        @endforeach
                        (whatsapp)
                    </p>
                    <div class="endereco">{!! $contato->endereco !!}</div>
                </div>
                <div class="col">
                    <a href="{{ route('produtos') }}" class="main-link">PRODUTOS</a>
                    <div class="categorias">
                        <a href="{{ route('produtos') }}">Todos os Produtos</a>
                        @foreach($categorias as $categoria)
                        <a href="{{ route('produtos', $categoria->slug) }}">
                            {{ $categoria->titulo }}
                        </a>
                        @endforeach
                    </div>
                </div>
                <div class="col">
                    <a href="{{ route('quem-somos') }}" class="main-link">QUEM SOMOS</a>
                    {{--
                    <a href="{{ route('ferramentaria') }}" class="main-link">FERRAMENTARIA</a>
                    --}}
                    <a href="{{ route('representantes') }}" class="main-link">REPRESENTANTES</a>
                    <a href="{{ route('fale-conosco') }}" class="main-link">FALE CONOSCO</a>
                </div>
                <div class="col">
                    <div class="links">
                        <a href="#" class="btn-busca">busca</a>
                        <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                        @foreach(['facebook', 'youtube', 'instagram', 'linkedin'] as $redeSocial)
                        @if($contato->{$redeSocial})
                        <a href="{{ $contato->{$redeSocial} }}" class="{{ $redeSocial }}" target="_blank">{{ $redeSocial }}</a>
                        @endif
                        @endforeach
                    </div>
                    <div class="copyright">
                        <p>
                            © {{ date('Y') }} Tasil Indústria Plástica<br>
                            Todos os direitos reservados.
                        </p>
                        <p>
                            <a href="http://www.trupe.net" target="_blank">Criação de sites</a>
                            <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
