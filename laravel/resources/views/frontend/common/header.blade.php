    <header @if(Route::currentRouteName() == 'home') class="home" @endif @if(Tools::isActive('produtos*')) style="border-bottom:none;height:75px" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
            <nav id="desktop">
                @include('frontend.common.nav')
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
            <div class="links">
                <a href="#" class="btn-busca">busca</a>
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                @foreach(['facebook', 'youtube', 'instagram', 'linkedin'] as $redeSocial)
                @if($contato->{$redeSocial})
                <a href="{{ $contato->{$redeSocial} }}" class="{{ $redeSocial }}" target="_blank">{{ $redeSocial }}</a>
                @endif
                @endforeach
            </div>
        </div>
        @unless(Tools::isActive('produtos*'))
        <div class="detalhe-borda">
            <div class="center">
                <div class="cor-1"></div>
                <div class="cor-2"></div>
                <div class="cor-3"></div>
                <div class="cor-4"></div>
                <div class="cor-5"></div>
            </div>
        </div>
        @endunless
        <nav id="mobile">
            <a href="{{ route('produtos') }}" @if(Tools::isActive('produtos*')) class="active" @endif>PRODUTOS</a>
            <a href="{{ route('quem-somos') }}" @if(Tools::isActive('quem-somos')) class="active" @endif>QUEM SOMOS</a>
            {{--
            <a href="{{ route('ferramentaria') }}" @if(Tools::isActive('ferramentaria')) class="active" @endif>FERRAMENTARIA</a>
            --}}
            <a href="{{ route('representantes') }}" @if(Tools::isActive('representantes')) class="active" @endif>REPRESENTANTES</a>
            <a href="{{ route('fale-conosco') }}" @if(Tools::isActive('fale-conosco')) class="active" @endif>FALE CONOSCO</a>
        </nav>
    </header>
