<div class="dropdown @if(Tools::isActive('produtos*')) active @endif">
    <a href="{{ route('produtos') }}">PRODUTOS</a>
    @unless(Tools::isActive('produtos*'))
    <nav>
        <div class="center">
            <a href="{{ route('produtos') }}">Todos os Produtos</a>
            @foreach($categorias as $categoria)
            <a href="{{ route('produtos', $categoria->slug) }}">{{ $categoria->titulo }}</a>
            @endforeach
        </div>
    </nav>
    @endunless
</div>
<a href="{{ route('quem-somos') }}" @if(Tools::isActive('quem-somos')) class="active" @endif>QUEM SOMOS</a>
{{--
<a href="{{ route('ferramentaria') }}" @if(Tools::isActive('ferramentaria')) class="active" @endif>FERRAMENTARIA</a>
--}}
<a href="{{ route('representantes') }}" @if(Tools::isActive('representantes')) class="active" @endif>REPRESENTANTES</a>
<a href="{{ route('fale-conosco') }}" @if(Tools::isActive('fale-conosco')) class="active" @endif>FALE CONOSCO</a>
