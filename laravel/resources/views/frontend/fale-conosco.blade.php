@extends('frontend.common.template')

@section('content')

    <div class="main fale-conosco">
        <div class="center">
            <h2 class="titulo">Fale Conosco</h2>

            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <div id="form-contato-response"></div>
                <input type="submit" value="ENVIAR">
            </form>

            <div class="info">
                <p class="telefones">
                    @foreach(explode(',', trim($contato->telefones)) as $telefone)
                        <span>
                            @foreach(explode(' ', trim($telefone)) as $part)
                            @if(strlen($part) <= 3)
                            {{ $part }}
                            @else
                            <strong>{{ $part }}</strong>
                            @endif
                            @endforeach
                        </span>
                    @endforeach
                </p>
                <p class="whatsapp">
                    @foreach(explode(' ', trim($contato->whatsapp)) as $part)
                    @if(strlen($part) <= 3)
                    {{ $part }}
                    @else
                    <strong>{{ $part }}</strong>
                    @endif
                    @endforeach
                    (whatsapp)
                </p>
                <div class="endereco">{!! $contato->endereco !!}</div>
            </div>
        </div>

        <div class="mapa">{!! $contato->google_maps !!}</div>
    </div>

@endsection
