@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        @if($banner->link)
        <a class="slide" href="{{ $banner->link }}" style="background-image: url({{ asset('assets/img/banners/'.$banner->fundo) }})">
            <div class="center">
                @if($banner->imagem)
                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                @endif
                @if($banner->texto)
                <div class="texto">
                    <span>{{ $banner->texto }}</span>
                </div>
                @endif
            </div>
        </a>
        @else
        <div class="slide" style="background-image: url({{ asset('assets/img/banners/'.$banner->fundo) }})">
            <div class="center">
                @if($banner->imagem)
                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                @endif
                @if($banner->texto)
                <div class="texto">
                    <span>{{ $banner->texto }}</span>
                </div>
                @endif
            </div>
        </div>
        @endif
        @endforeach
        <div class="cycle-pager" style="z-index: 9999;"></div>
    </div>

    <div class="chamadas">
        <div class="center">
            @foreach(range(1, 3) as $i)
            <a href="{{ $chamadas->{'link_'.$i} }}" class="chamada-{{ $i }}" style="background-image: url({{ asset('assets/img/chamadas/'.$chamadas->{'imagem_'.$i}) }})">
                <div class="texto-wrapper">
                    <div class="texto">
                        <h3>{{ $chamadas->{'titulo_'.$i} }}</h3>
                        <p>{{ $chamadas->{'subtitulo_'.$i} }}</p>
                    </div>
                </div>
                <div class="overlay">
                    <span>ver detalhes &raquo;</span>
                </div>
            </a>
            @endforeach
        </div>
    </div>

    <div class="promocoes">
        <div class="center">
            @if(count($promocoes))
            <h3>Confira nossas Promoções</h3>
            <div class="promocoes-thumbs">
                @foreach($promocoes as $produto)
                <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">
                    <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                    <div class="overlay">
                        <span>ver detalhes &raquo;</span>
                    </div>
                </a>
                @endforeach
            </div>
            @elseif(count($destaques))
            <h3>Produtos em destaque</h3>
            <div class="promocoes-thumbs">
                @foreach($destaques as $produto)
                <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">
                    <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                    <div class="overlay">
                        <span>ver detalhes &raquo;</span>
                    </div>
                </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>

@endsection
