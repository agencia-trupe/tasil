@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <h2 class="titulo">Resultados de busca: {{ $termo }}</h2>
            <div class="produtos-thumbs">
                @foreach($produtos as $produto)
                <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">
                    <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                    <span>{{ $produto->titulo }}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
