@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="produtos-categorias">
            <div class="center">
                <a href="{{ route('produtos') }}" @if(!$categoria) class="active" @endif>Todos os Produtos</a>
                @foreach($categorias as $c)
                <a href="{{ route('produtos', $c->slug) }}" @if(isset($categoria) && $c->slug == $categoria->slug) class="active" @endif>{{ $c->titulo }}</a>
                @endforeach
            </div>
        </div>
        <div class="center">
            <h2 class="titulo">
                {{ $categoria ? $categoria->titulo : 'Todos os Produtos' }}
                @if($catalogo->catalogo)
                <a href="{{ url('assets/catalogo/'.$catalogo->catalogo) }}" target="_blank" class="catalogo">
                    Download do CATÁLOGO COMPLETO
                </a>
                @endif
            </h2>
            <div class="produtos-thumbs">
                @foreach($produtos as $produto)
                <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">
                    <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                    <span>{{ $produto->titulo }}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
