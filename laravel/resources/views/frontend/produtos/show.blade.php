@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="produtos-categorias">
            <div class="center">
                <a href="{{ route('produtos') }}" @if(!$categoria) class="active" @endif>Todos os Produtos</a>
                @foreach($categorias as $c)
                <a href="{{ route('produtos', $c->slug) }}" @if(isset($categoria) && $c->slug == $categoria->slug) class="active" @endif>{{ $c->titulo }}</a>
                @endforeach
            </div>
        </div>
        <div class="center">
            <h2 class="titulo">{{ $produto->categoria->titulo }} | {{ $produto->titulo }}</h2>
            <div class="produtos-show">
                @if(count($produto->imagens))
                <div class="produto-imagem">
                    <img src="{{ asset('assets/img/produtos/imagens/'.$produto->imagens->first()->imagem) }}" alt="">
                </div>
                @endif

                <div class="informacoes">
                    <h3>Sugestão de uso:</h3>
                    <p>{{ $produto->sugestao_de_uso }}</p>

                    <h5>{{ $produto->descricao }}</h5>

                    <div class="caracteristica">
                        <label>dimensões</label>
                        <span>{{ $produto->dimensoes }}</span>
                    </div>
                    <div class="caracteristica">
                        <label>cor</label>
                        <span>{{ $produto->cor }}</span>
                    </div>

                    @if(count($produto->imagens) > 1)
                    <div class="produto-imagens-thumbs">
                        @foreach($produto->imagens as $imagem)
                            <a href="#" data-imagem="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}" @if($produto->imagens->first() == $imagem) class="active" @endif>
                                <img src="{{ asset('assets/img/produtos/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                    @endif
                </div>

                <div class="voltar">
                    <a href="{{ route('produtos', $produto->categoria->slug) }}">&laquo; voltar</a>
                </div>
            </div>
        </div>
    </div>

@endsection
