@extends('frontend.common.template')

@section('content')

    <div class="main quem-somos">
        <div class="center">
            <h2 class="titulo">Quem Somos</h2>

            <div class="texto" style="background-image: url({{ asset('assets/img/quem-somos/'.$quemSomos->imagem) }})">{!! $quemSomos->texto !!}</div>
        </div>

        <div class="boxes">
            <div class="center">
                <div class="missao">
                    <h3>Missão</h3>
                    <p>{{ $quemSomos->missao }}</p>
                </div>
                <div class="visao">
                    <h3>Visão</h3>
                    <p>{{ $quemSomos->visao }}</p>
                </div>
                <div class="valores">
                    <h3>Valores</h3>
                    <p>{{ $quemSomos->valores }}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
