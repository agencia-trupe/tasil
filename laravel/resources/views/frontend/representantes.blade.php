@extends('frontend.common.template')

@section('content')

    <div class="main representantes">
        <div class="center">
            <h2 class="titulo">Representantes</h2>

            <div class="lista">
                <p>ENCONTRE O SEU REPRESENTANTE:</p>
                <?php
                    $estadosCadastrados = \App\Models\Representante::orderBy('estado', 'ASC')->pluck('estado')->toArray();
                    $estados = [];
                    foreach ($estadosCadastrados as $uf) {
                        $estados[$uf] = \Tools::listaEstados()[$uf];
                    }
                ?>
                {!! Form::select('estado', $estados, $estado, ['id' => 'representantes-estados', 'placeholder' => 'Selecione o Estado']) !!}

                @if(isset($representantes))
                <div class="representantes-lista">
                    @if(!count($representantes))
                        <p>NÃO HÁ REPRESENTANTES NESSE ESTADO. POR FAVOR, SELECIONE UM ESTADO VIZINHO.</p>
                    @else
                        @foreach($representantes as $representante)
                        <div>
                            @if($representante->imagem)
                            <img src="{{ asset('assets/img/representantes/'.$representante->imagem) }}" alt="">
                            <div class="texto">
                                <h3>{{ $representante->titulo }}</h3>
                                {!! $representante->informacoes !!}
                            </div>
                            @else
                            <div class="texto" style="width:100%!important">
                                <h3>{{ $representante->titulo }}</h3>
                                {!! $representante->informacoes !!}
                            </div>
                            @endif
                        </div>
                        @endforeach
                    @endif
                </div>
                @endif
            </div>

            <div class="mapa">
                @foreach(Tools::listaEstados() as $uf => $titulo)
                <a href="{{ route('representantes', ['uf' => $uf]) }}" class="{{ strtolower($uf) }} @if($estado == $uf) active @endif" title="{{ $titulo  }}">{{ $titulo }}</a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
