@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('fundo', 'Fundo') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->fundo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('fundo', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (opcional)') !!}
@if($submitText == 'Alterar' && $registro->imagem)
    <img src="{{ url('assets/img/banners/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto (opcional)') !!}
    {!! Form::text('texto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link (opcional)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
