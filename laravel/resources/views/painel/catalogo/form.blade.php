@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('catalogo', 'Catálogo') !!}
    @if($registro->catalogo)
    <p>
        <a href="{{ url('assets/catalogo/'.$registro->catalogo) }}" target="_blank" style="display:block;">{{ $registro->catalogo }}</a>
    </p>
    @endif
    {!! Form::file('catalogo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
