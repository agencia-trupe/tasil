@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1 (235x235px)') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_1', 'Título 1') !!}
            {!! Form::text('titulo_1', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('subtitulo_1', 'Subtítulo 1') !!}
            {!! Form::text('subtitulo_1', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_1', 'Link 1') !!}
            {!! Form::text('link_1', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2 (235x235px)') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_2', 'Título 2') !!}
            {!! Form::text('titulo_2', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('subtitulo_2', 'Subtítulo 2') !!}
            {!! Form::text('subtitulo_2', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_2', 'Link 2') !!}
            {!! Form::text('link_2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3 (235x235px)') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_3', 'Título 3') !!}
            {!! Form::text('titulo_3', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('subtitulo_3', 'Subtítulo 3') !!}
            {!! Form::text('subtitulo_3', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_3', 'Link 3') !!}
            {!! Form::text('link_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
