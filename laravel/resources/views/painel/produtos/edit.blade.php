@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Produto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
