@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produtos_categoria_id', 'Categoria') !!}
    {!! Form::select('produtos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('sugestao_de_uso', 'Sugestão de uso') !!}
    {!! Form::textarea('sugestao_de_uso', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
</div>

<div class="form-group">
    {!! Form::label('dimensoes', 'Dimensões') !!}
    {!! Form::text('dimensoes', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cor', 'Cor') !!}
    {!! Form::text('cor', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <div class="checkbox">
        <label>
            <input type="hidden" name="promocao" value="0">
            {!! Form::checkbox('promocao', 1) !!} Produto em promoção
        </label>
    </div>
</div>

<div class="form-group">
    <div class="checkbox">
        <label>
            <input type="hidden" name="destaque" value="0">
            {!! Form::checkbox('destaque', 1) !!} Produto em destaque na Home (é exibido apenas quando não houverem promoções)
        </label>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
