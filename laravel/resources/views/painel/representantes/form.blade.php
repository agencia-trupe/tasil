@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('estado', 'Estado') !!}
    {!! Form::select('estado', Tools::listaEstados(), null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/representantes/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('informacoes', 'Informações') !!}
    {!! Form::textarea('informacoes', null, ['class' => 'form-control ckeditor', 'data-editor' => 'representantes']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.representantes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
