-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 23/05/2017 às 17:02
-- Versão do servidor: 5.7.17
-- Versão do PHP: 7.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `tasil`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `fundo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `fundo`, `imagem`, `texto`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, 'pexels-photo-90911_20170523162809.jpeg', 'banner_20170523162810.png', 'Grande variedade de produtos plásticos para festas e eventos', '', '2017-05-23 16:28:10', '2017-05-23 16:28:10');

-- --------------------------------------------------------

--
-- Estrutura para tabela `chamadas`
--

CREATE TABLE `chamadas` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `imagem_1`, `titulo_1`, `subtitulo_1`, `link_1`, `imagem_2`, `titulo_2`, `subtitulo_2`, `link_2`, `imagem_3`, `titulo_3`, `subtitulo_3`, `link_3`, `created_at`, `updated_at`) VALUES
(1, 'caixa-estrela_20170523164409.png', 'CAIXINHA ESTRELA', 'tampa presente', '#', 'caixa-quadrada_20170523164410.png', 'CAIXINHA QUADRADA', 'tampa presente', '#', 'tubo_20170523164410.png', 'TUBO', '8cm - metal', '#', NULL, '2017-05-23 16:44:10');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefones`, `whatsapp`, `endereco`, `google_maps`, `facebook`, `youtube`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', '+55 11 2011 2677, +55 11 2019 2902', '+55 11 98108 5795', 'Rua Ettore Andreazza, 383 - Jardim Tiet&ecirc; | S&atilde;o Mateus<br />\r\n03944-000 - S&atilde;o Paulo - SP<br />\r\n<em>Pr&oacute;ximo &agrave; Av. Sapopemba</em>', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.081165361695!2d-46.4958133850214!3d-23.601421884663033!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce67c3a16afc0d%3A0x596b71aeefaaf787!2sRua+Ettore+Andreazza%2C+383+-+Jardim+Tiete%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1495558059212\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '#', '#', '#', NULL, '2017-05-23 16:47:46');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_05_19_024007_create_banners_table', 1),
('2017_05_19_024434_create_chamadas_table', 1),
('2017_05_19_025001_create_quem_somos_table', 1),
('2017_05_19_025835_create_representantes_table', 1),
('2017_05_19_031039_create_produtos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `produtos_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sugestao_de_uso` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `dimensoes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promocao` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtos`
--

INSERT INTO `produtos` (`id`, `produtos_categoria_id`, `ordem`, `slug`, `titulo`, `capa`, `sugestao_de_uso`, `descricao`, `dimensoes`, `cor`, `promocao`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'exemplo', 'Exemplo', 'im1_20170523165012.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Phasellus feugiat leo nec erat tincidunt euismod. Cras elit ante, consectetur maximus pulvinar id, aliquam nec turpis. Mauris at aliquam velit, eu pulvinar tortor.', '10 x 10 cm', 'natural', 1, '2017-05-23 16:50:13', '2017-05-23 16:50:13');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos_categorias`
--

CREATE TABLE `produtos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tubo', 'tubo', '2017-05-23 16:48:17', '2017-05-23 16:48:17'),
(2, 2, 'Latinha', 'latinha', '2017-05-23 16:48:23', '2017-05-23 16:48:23'),
(3, 3, 'Panelinha', 'panelinha', '2017-05-23 16:48:28', '2017-05-23 16:48:28'),
(4, 6, 'Tela Mágica', 'tela-magica', '2017-05-23 16:48:33', '2017-05-23 16:48:33'),
(5, 5, 'Baleiras', 'baleiras', '2017-05-23 16:48:42', '2017-05-23 16:48:42'),
(6, 4, 'Caixinha', 'caixinha', '2017-05-23 16:48:46', '2017-05-23 16:48:46'),
(7, 9, 'Base', 'base', '2017-05-23 16:48:51', '2017-05-23 16:48:51'),
(8, 7, 'Balde', 'balde', '2017-05-23 16:49:01', '2017-05-23 16:49:08'),
(9, 8, 'Espiral', 'espiral', '2017-05-23 16:49:04', '2017-05-23 16:49:04');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos_imagens`
--

CREATE TABLE `produtos_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `produto_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtos_imagens`
--

INSERT INTO `produtos_imagens` (`id`, `produto_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'im2_20170523165024.png', '2017-05-23 16:50:24', '2017-05-23 16:50:24'),
(2, 1, 0, 'im1_20170523165024.png', '2017-05-23 16:50:24', '2017-05-23 16:50:24');

-- --------------------------------------------------------

--
-- Estrutura para tabela `quem_somos`
--

CREATE TABLE `quem_somos` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `missao` text COLLATE utf8_unicode_ci NOT NULL,
  `visao` text COLLATE utf8_unicode_ci NOT NULL,
  `valores` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `quem_somos`
--

INSERT INTO `quem_somos` (`id`, `texto`, `missao`, `visao`, `valores`, `created_at`, `updated_at`) VALUES
(1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae leo non nunc dictum molestie ac bibendum enim. Etiam posuere dapibus magna, quis tempor odio vulputate sit amet. Aliquam faucibus, purus a vulputate vehicula, ex arcu imperdiet nisl, eget porta nulla dui ut eros.</p>\r\n\r\n<p>Mauris accumsan ultrices arcu, at fermentum nulla finibus eget. Cras ut justo eget nulla imperdiet efficitur. Quisque posuere nec nisi at vulputate. Ut semper fermentum tortor, vulputate luctus eros vulputate molestie. Nam placerat felis id sagittis rhoncus.</p>\r\n\r\n<p>Maecenas gravida faucibus arcu et posuere. Nunc a sagittis lacus. Aenean aliquam dui at libero interdum, ut malesuada nibh venenatis.</p>\r\n', 'Maecenas gravida faucibus arcu et posuere. Nunc a sagittis lacus.', 'Ut feugiat ac tellus sit amet porta. In diam tortor, congue in sem a, pellentesque placerat lacus. Vivamus pharetra tellus eu sapien congue, ac ornare sem condimentum.', 'Fusce placerat risus in consectetur accumsan. In hac habitasse platea dictumst. Proin condimentum turpis at erat venenatis tincidunt. ', NULL, '2017-05-23 16:45:23');

-- --------------------------------------------------------

--
-- Estrutura para tabela `representantes`
--

CREATE TABLE `representantes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `informacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `representantes`
--

INSERT INTO `representantes` (`id`, `ordem`, `estado`, `imagem`, `titulo`, `informacoes`, `created_at`, `updated_at`) VALUES
(1, 0, 'SP', 'abstract-art-depths-of-emotion_20170523164558.jpg', 'Exemplo', '<p>Informa&ccedil;&otilde;es de contato</p>\r\n', '2017-05-23 16:45:58', '2017-05-23 16:45:58'),
(2, 0, 'SP', 'pexels-photo-90911_20170523164609.jpeg', 'Exemplo', '<p>Informa&ccedil;&otilde;es de contato</p>\r\n', '2017-05-23 16:46:09', '2017-05-23 16:46:09');

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$f5yfy0o4u3OyHCe3LxaTTe4CKKyzlVbQG8nMKN8Y841FYa93ixH4.', NULL, NULL, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `chamadas`
--
ALTER TABLE `chamadas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_produtos_categoria_id_foreign` (`produtos_categoria_id`);

--
-- Índices de tabela `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_imagens_produto_id_foreign` (`produto_id`);

--
-- Índices de tabela `quem_somos`
--
ALTER TABLE `quem_somos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `representantes`
--
ALTER TABLE `representantes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `chamadas`
--
ALTER TABLE `chamadas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de tabela `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `quem_somos`
--
ALTER TABLE `quem_somos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `representantes`
--
ALTER TABLE `representantes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `produtos_produtos_categoria_id_foreign` FOREIGN KEY (`produtos_categoria_id`) REFERENCES `produtos_categorias` (`id`) ON DELETE SET NULL;

--
-- Restrições para tabelas `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  ADD CONSTRAINT `produtos_imagens_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
